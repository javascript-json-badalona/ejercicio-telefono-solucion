class Telefono {
  constructor() {
    this.timer = null;
    this.numero = '';
    this.maxDigitos = 9;
    this.tecladoBloqueado = false;
    this.asignaElementosHTML();
    this.asignaListeners();
  }

  isLongitudMaxima() {
    return this.numero.length >= this.maxDigitos;
  }

  refrescaDisplay() {
    this.display.textContent = this.numero;
  }

  asignaListeners() {
    for (let tecla of this.teclas) {
      tecla.addEventListener('click', () => {
        if (this.isLongitudMaxima() || this.tecladoBloqueado) {
          return false;
        }
        const numero = tecla.textContent;
        this.numero += numero;
        this.refrescaDisplay();
        this.checkBotonLlamar();
      });
    }
    this.botonBorrar.addEventListener('click', () => {
      if (this.tecladoBloqueado) {
        return false;
      }
      this.numero = this.numero.slice(0, -1);
      this.refrescaDisplay();
      this.checkBotonLlamar();
    });
    this.botonLlamar.addEventListener('click', e => {
      e.preventDefault();
      if (!this.botonLlamar.classList.contains('activo')) {
        return false;
      }
      this.compruebaExistente(this.numero);
    });
    this.botonColgar.addEventListener('click', e => {
      e.preventDefault();
      this.colgar();
    });
  }

  muestraError() {
    this.error.classList.remove('off');
    setTimeout(() => {
      this.error.classList.add('off');
    }, 2000);
  }

  compruebaExistente(numero) {
    fetch('http://localhost:3000/telefonos').then(resp => resp.json()).then(datos => {
      if (datos.some(dato => dato.numero === numero)) {
        this.llamar();
      } else {
        this.muestraError();
      }
    });
  }

  llamar() {
    this.botonLlamar.classList.add('off');
    this.botonColgar.classList.remove('off');
    this.mensaje.classList.remove('off');
    this.tecladoBloqueado = true;
    this.timer = setTimeout(() => {
      this.colgar();
      console.log('Llamada finalizada automáticamente');
    }, 5000);
  }

  colgar() {
    this.botonLlamar.classList.remove('off');
    this.botonColgar.classList.add('off');
    this.tecladoBloqueado = false;
    this.mensaje.classList.add('off');
    this.numero = '';
    this.refrescaDisplay();
    clearTimeout(this.timer);
  }

  checkBotonLlamar() {
    if (this.isLongitudMaxima()) {
      this.botonLlamar.classList.add('activo');
    } else {
      this.botonLlamar.classList.remove('activo');
    }
  }

  asignaElementosHTML() {
    this.mensaje = document.querySelector('.mensaje');
    this.teclas = document.querySelectorAll('.botonera button');
    this.botonBorrar = document.querySelector('.borrar');
    this.display = document.querySelector('.numero');
    this.botonLlamar = document.querySelector('.llamar');
    this.botonColgar = document.querySelector('.colgar');
    this.error = document.querySelector('.error');
  }
}

const telefono = new Telefono();
